#!/usr/bin/env bash

apt-get update

# Install/Configure Apache
apt-get install -y apache2
mkdir /vagrant/www
rm -rf /var/www/html
ln -fs /vagrant/www /var/www/html

# Install PHP
apt-get install -y php5 php5-mysql libapache2-mod-php5 php5-mcrypt
php5enmod mcrypt

# Install MySQL

debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password root'

apt-get install -y mysql-server mysql-client


# Install composer

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer 

# Setup Laravel

cd /vagrant/www
composer create-project laravel/laravel --prefer-dist

# restart apache to reload mcrypt mod

service apache2 restart

